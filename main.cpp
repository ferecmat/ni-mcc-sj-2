#include <algorithm>
#include <atomic>
#include <cstdint>
#include <cstring>
#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string_view>
#include <unordered_set>
#include <utility>
#include <vector>

#include <omp.h>

typedef struct Node {
    uint64_t hash;
    char values[16];
    struct Node *next;
} TNode;

TNode *new_Node(TNode *array, int i, uint64_t hash, char *val, TNode *next) {
    TNode *n;

    n = array + i;
    n->hash = hash;
    memcpy(n->values, val, 16);
    n->next = next;
    return n;
}

/*

Muzete si vytorit novy datovy typ pro polozku hash tabulky, ale musi obsahovat
minimalne polozky "hash","values" a "next"

*/

uint64_t gen_hash(char *x) {
    uint64_t a;
    a = x[0] ^ x[4] ^ x[8] ^ x[12];
    a <<= 8;
    a = x[1] ^ x[5] ^ x[9] ^ x[13];
    a <<= 8;
    a = x[2] ^ x[6] ^ x[10] ^ x[14];
    a <<= 8;
    a = x[3] ^ x[7] ^ x[11] ^ x[15];
    return a;
}

double generate(uint64_t n, char *values, uint64_t B, int *use2, int *bs) {

    TNode **buckets;
    TNode *array;
    TNode *last_node_ptr;
    int used = 0;

    // init table
    buckets = (TNode **)malloc(B * sizeof(TNode *));
    for (int i = 0; i < B; i++)
        buckets[i] = NULL;

    // bucket sizes
    for (int i = 0; i < B; i++)
        bs[i] = 0;

    array = (TNode *)malloc(n * sizeof(TNode));
    int found;
    double start = omp_get_wtime();

    for (uint64_t i = 0; i < n; i++) {
        found = 0;
        uint64_t hash = gen_hash(values + 16 * i);
        uint64_t bucket = hash % B;

        TNode *node = buckets[bucket];
        last_node_ptr = node;
        while (node) {
            if ((hash == node->hash) &&
                (memcmp(values + 16 * i, node->values, 16) == 0)) {
                found = 1;
                break;
            }
            last_node_ptr = node;
            node = node->next;
        }
        if (found)
            continue;
        // append node:

        node = new_Node(array, used, hash, values + 16 * i, NULL);
        if (last_node_ptr)
            last_node_ptr->next = node;
        else
            buckets[bucket] = node;
        bs[bucket]++;
        used++;
    }

    double duration = omp_get_wtime() - start;

    printf("Used=%d\n", used);
    *use2 = used;
    free(buckets);
    free(array);
    return duration;
}

/*

Napiste zde paralelni verzi fce generate()

*/

struct SpinLock {
    auto lock() -> void {
        while (flag_.test_and_set(std::memory_order_acquire))
            ;
    }

    auto unlock() -> void { flag_.clear(std::memory_order_release); }

  private:
    std::atomic_flag flag_{false};
};

double generate2(uint64_t n, char *values, uint64_t B, int *use2, int *bs) {
    std::vector<std::unordered_set<std::string_view>> table(B);
    std::vector<SpinLock> locks{B};

    memset(bs, 0, sizeof(int) * B);

    double start = omp_get_wtime();

#pragma omp parallel for num_threads(8)
    for (uint64_t i = 0; i < n; i++) {
        uint64_t hash = gen_hash(values + 16 * i);
        uint64_t bucket = hash % B;

        locks[bucket].lock();
        table[bucket].insert(std::string_view(values + 16 * i, 16));
        locks[bucket].unlock();
    }

    int used = 0;
#pragma omp parallel for reduction(+ : used)
    for (int i = 0; i < B; i++) {
        bs[i] = table[i].size();
        used += bs[i];
    }

    double duration = omp_get_wtime() - start;

    *use2 = used;
    return duration;
}

double test(int n, char *values, int B) {
    int use1, use2;
    int *bs1;
    double x;
    bs1 = (int *)malloc(B * sizeof(int));
    for (int i = 0; i < B; i++)
        bs1[i] = 0;
    x = generate(n, values, B, &use1, bs1);
    printf("%d %d=%g\n", n, B, x);
    int *bs2;
    bs2 = (int *)malloc(B * sizeof(int));
    for (int i = 0; i < B; i++)
        bs2[i] = 0;
    x = generate2(n, values, B, &use2, bs2);
    printf("%d %d=%g\n", n, B, x);
    if (use1 != use2) {
        printf("Chyba1 %d %d\n", use1, use2);
        x = 1e7;
    } else {
        for (uint64_t b = 0; b < B; b++) {
            if (bs2[b] != bs1[b]) {
                printf("Chyba [%d]: %d %d\n", b, bs1[b], bs2[b]);
                x = 1e7;
            }
        }
    }
    free(bs1);
    free(bs2);
    return x;
}

int main() {
    int i, n = 1000000;
    int B;
    double x, sum;
    char *values;
    values = (char *)malloc(16 * n);
    for (i = 0; i < (16 * n); i++)
        values[i] = (i % 101) ^ (i % 255);
    // x=generate(n,values,B);
    // printf("%d %d=%g\n",n,B,x);
    B = 97;
    sum = test(n, values, B);
    B = 13;
    sum += test(n, values, B);
    printf("Total %g time\n", sum);
    if (sum > 8.0) {
        printf("0 SJ2 points\n");
    } else {
        x = 12.0 * 0.763 / sum;
        if (x > 15)
            x = 15;
        printf("%g SJ2 points\n", x);
    }
};
