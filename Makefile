sj: main.cpp Makefile
	g++ -O3 -march=broadwell -fopenmp -ffast-math -std=c++17 -ftree-vectorize main.cpp -o sj

clean:
	rm -f sj run.* core.*

run: clean sj
	qrun 20c 1 pdp_fast run
